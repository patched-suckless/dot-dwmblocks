#!/usr/bin/env bash

percent(){
    cat /sys/class/power_supply/BAT0/capacity
}

discharge_symbol(){
    amount=$(cat /sys/class/power_supply/BAT0/capacity)
    ([[ $amount -le 10 ]] && echo "") || \
        ([[ $amount -le 20 ]] && echo "") || \
        ([[ $amount -le 30 ]] && echo "") || \
        ([[ $amount -le 40 ]] && echo "") || \
        ([[ $amount -le 50 ]] && echo "") || \
        ([[ $amount -le 60 ]] && echo "") || \
        ([[ $amount -le 70 ]] && echo "") || \
        ([[ $amount -le 80 ]] && echo "") || \
        ([[ $amount -le 90 ]] && echo "") || \
        ([[ $amount -le 100 ]] && echo "")
}

status(){
    stat=$(cat /sys/class/power_supply/BAT0/status)
    [[ $stat == "Charging" ]] && echo ""
    [[ $stat == "Unknown" ]] && echo ""
    [[ $stat == "Discharging" ]] && echo "$(discharge_symbol)"
    [[ $stat == "Full" ]] && echo ""
}

echo "$(status) $(percent)%"
