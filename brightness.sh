#!/usr/bin/env bash

percentage(){
    amount=$(xbacklight -get)
    echo $amount | sed -e 's/\..*$//'
}

echo " $(percentage)%"
