#!/usr/bin/env bash

temperature(){
    $(cat /sys/class/thermal/thermal_zone0/temp | sed -e 's/...$//')
}

echo " $(temperature)"
